$(function(){
	$projects = $('.projects')

	$projects.isotope({
	  itemSelector: '.project'
	});
})

$('.filters .btn').click(function(){

	var $this = $(this)
		,filter = $this.data('filter')

	$this.siblings('.btn-primary').removeClass('btn-primary')
	$this.addClass('btn-primary')

	$projects.isotope({
		filter:function(){
			if(filter=='all')
				return true
			else if($(this).data('filters').split(' ').indexOf(filter)==-1)
				return false
			else
				return true
		}
	})
})