<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ScoreStoriesCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'aakil:score-stories';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$id = $this->argument('id');
		$pageSize = 5;

		while(Story::where('id','>=',$id)->limit($pageSize)->count()>0){

			$stories = Story::where('id','>=',$id)->limit($pageSize)->get();
			if($stories->count()==0) exit;

			foreach($stories as $story){ var_dump($story->id);
				sleep(1);
				try{
					$story->score = $story->api()->points;
				}catch(Exception $e){
					var_dump($e->getMessage());
					$story->delete();
					continue;
				}
				
				$story->save();
			}

			$id=$story->id+1;
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('id', InputArgument::OPTIONAL, 'An example argument.',0),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
