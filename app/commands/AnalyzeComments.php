<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class AnalyzeComments extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'aakil:analyze-comments';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$comments = Comment::all();
		$endpoint = "http://access.alchemyapi.com/calls/html/HTMLGetTargetedSentiment";
		$targets = array('facebook','google','amazon','twitter','apple','microsoft');

		foreach($comments as $comment){

			if(CommentSentiment::where('comment_id',$comment->id)->count()>0) continue;

			foreach($targets as $target){
			$html = $comment->data->comment_text;

			if(!stristr($html, $target)) continue;

			$queryString = http_build_query(array(
				'apikey'=>getenv('alchemyKey')
				,'html' =>$html
				,'target'=>$target
				,'outputMode'=>'json'
			));

			$response = json_decode(@file_get_contents($endpoint.'?'.$queryString));
			if(!$response) continue;

			$sentiment = new CommentSentiment;
			$sentiment->comment_id = $comment->id;
			$sentiment->target = $target;
			$sentiment->data = $response;
			$sentiment->save();

		}}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
