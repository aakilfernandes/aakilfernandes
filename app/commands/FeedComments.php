<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class FeedComments extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'aakil:feed-comments';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$query = $this->argument('query');

		for($page=0;$page<50;$page++){

			$url = "https://hn.algolia.com/api/v1/search_by_date?tags=comment&query=$query&page=$page";

			$response = json_decode(file_get_contents($url));
			$hits = $response->hits;

			foreach ($hits as $hit) {
				$id = $hit->objectID;

				if(Comment::where('id',$id)->count()>0)
				continue;
				
				$comment = new Comment;
				$comment->id = $id;
				$comment->data = $hit;
				$comment->save();
			}

		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('query', InputArgument::REQUIRED, 'query.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
