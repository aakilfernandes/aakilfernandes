<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class FillStoriesCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'aakil:fill-stories';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$pageSize = 100;
		$id=$this->argument('id');

		while(Story::where('id','>=',$id)->limit($pageSize)->count()>0){

			$stories = Story::where('id','>=',$id)->limit($pageSize)->get();
			if($stories->count()==0) exit;

			foreach($stories as $story){ 
				$story->title=$story->data->title;
				$story->domain = $story->domain();
				$story->hourOfWeek = $story->hourOfWeek();
				$story->save();
			}

			$id=$story->id+1;
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('id', InputArgument::OPTIONAL, 'An example argument.',0),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
