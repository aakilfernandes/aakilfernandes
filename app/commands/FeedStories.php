<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class FeedStories extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'aakil:feed-stories';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		for($page=0;$page<50;$page++){

			$url = 'https://hn.algolia.com/api/v1/search_by_date?tags=story&page='.$page;

			$response = json_decode(file_get_contents($url));
			$hits = $response->hits;

			foreach ($hits as $hit) {
				$id = $hit->objectID;

				if(Story::where('id',$id)->count()>0)
					exit;
				
				$story = new Story;
				$story->id = $id;
				$story->data = $hit;
				$story->save();
			}

		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
