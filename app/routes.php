<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('*',function(){
	return Redirect::to('http://aakilfernandes.github.io',301);
});

Route::get('/', function()
{
	return Redirect::to('http://aakilfernandes.github.io',301);
	return View::make('index');
});

Route::get('/resume', function()
{
	$file= public_path(). "/files/AakilFernandes.Resume.pdf";
    $headers = array(
          'Content-Type: application/pdf',
        );
    return Response::download($file, 'AakilFernandes.Resume.pdf', $headers);
});

Route::get('/blog/{slug}', function($slug)
{
	return View::make("blog/$slug");
});