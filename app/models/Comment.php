<?php

class Comment extends \Eloquent {
	protected $fillable = [];

	protected $appends = ['data'];

	public function setDataAttribute($value){
		$this->attributes['data']=json_encode($value);
	}

	public function getDataAttribute(){
		return json_decode($this->attributes['data']);
	}
}