<?php

class Story extends \Eloquent {

	use SoftDeletingTrait;
	
	protected $fillable = [];

	protected $appends = ['data'];

	public function setDataAttribute($value){
		$this->attributes['data']=json_encode($value);
	}

	public function getDataAttribute(){
		return json_decode($this->attributes['data']);
	}

	public function api(){
		return json_decode(file_get_contents("https://hn.algolia.com/api/v1/items/{$this->id}"));
	}

	public function hourOfWeek(){
		//$timestamp = $this->data->created_at;
		//$timestamp = str_ireplace('T',' ', $timestamp);
		//$timestamp = str_ireplace('.000Z','', $timestamp);

		$dayOfWeek = date('N', strtotime($this->data->created_at));
		$hourOfDay = date('H', strtotime($this->data->created_at));

		return (($dayOfWeek-1)*24)+$hourOfDay;
	}

	public function domain(){
		try{
			$domain = utf8_encode(parse_url($this->data->url)['host']);
			$domain = str_ireplace('www.','',$domain);
			return $domain;
		}catch(Exception $e){
			return null;
		}
	}
}