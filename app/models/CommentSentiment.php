<?php

class CommentSentiment extends \Eloquent {
	protected $fillable = [];
	protected $appends = ['data'];
	protected $attributes = ['score'=>0];

	public function getDataAttribute(){
		return json_decode($this->attributes['data']);
	}

	public function setDataAttribute($value){
		$this->attributes['data']=json_encode($value);
	}
}