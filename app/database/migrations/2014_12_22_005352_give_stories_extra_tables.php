<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GiveStoriesExtraTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('stories',function($table){
			$table->string('domain')->after('score')->nullable();
			$table->integer('hourOfWeek')->after('score')->nullable();
			$table->string('title')->after('score')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('stories',function($table){
			$table->dropColumn(['isListicle','domain','hourofWeek','title']);
		});
	}

}
