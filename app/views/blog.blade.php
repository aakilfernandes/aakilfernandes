<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    {{HTML::style('http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600')}}
    {{HTML::style('//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.2.0/css/bootstrap.min.css')}}
    {{HTML::style('//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css')}}
    {{HTML::style('/css/style.css')}}
    {{HTML::style('/css/page/blog.css')}}
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    {{HTML::script('/js/lib/ga.js')}}
    <header>
        <div class="container">
            <a href="/"><h1>Aakil Fernandes</h1></a>
        </div>
    </header>
    <div class="container buttons">
      <a class="sbg-button sbg-button-facebook"  data-sbg-network="facebook"  data-sbg-url="{{Request::url()}}"  data-sbg-title="@yield('title')"  data-sbg-summary=""  data-sbg-width="600"  data-sbg-height="368" >
        <i class="sbg-button-icon fa fa-facebook"></i>
      </a>
            
      <a class="sbg-button sbg-button-twitter"  data-sbg-network="twitter"  data-sbg-text="@yield('title') {{Request::url()}}"  data-sbg-via="aakilfernandes"  data-sbg-width="600"  data-sbg-height="258" >
        <i class="sbg-button-icon fa fa-twitter"></i>
      </a>
            
      <a class="sbg-button sbg-button-linkedin"  data-sbg-network="linkedin"  data-sbg-url="{{Request::url()}}"  data-sbg-title="@yield('title')"  data-sbg-source="Aakil Fernandes"  data-sbg-summary=""  data-sbg-width="585"  data-sbg-height="471" >
        <i class="sbg-button-icon fa fa-linkedin"></i>
      </a>
            
      <a class="sbg-button sbg-button-google-plus"  data-sbg-network="google-plus"  data-sbg-url="{{Request::url()}}"  data-sbg-width="500"  data-sbg-height="505" >
        <i class="sbg-button-icon fa fa-google-plus"></i>
      </a>
            
      <a class="sbg-button sbg-button-email"  data-sbg-network="email"  data-sbg-subject="@yield('title')"  data-sbg-body="{{Request::url()}}" >
        <i class="sbg-button-icon fa fa-envelope"></i>
      </a>
    </div>
    @yield('content')
    <div class="container buttons">
      <a class="sbg-button sbg-button-facebook"  data-sbg-network="facebook"  data-sbg-url="{{Request::url()}}"  data-sbg-title="@yield('title')"  data-sbg-summary=""  data-sbg-width="600"  data-sbg-height="368" >
        <i class="sbg-button-icon fa fa-facebook"></i>
      </a>
            
      <a class="sbg-button sbg-button-twitter"  data-sbg-network="twitter"  data-sbg-text="@yield('title') {{Request::url()}}"  data-sbg-via="aakilfernandes"  data-sbg-width="600"  data-sbg-height="258" >
        <i class="sbg-button-icon fa fa-twitter"></i>
      </a>
            
      <a class="sbg-button sbg-button-linkedin"  data-sbg-network="linkedin"  data-sbg-url="{{Request::url()}}"  data-sbg-title="@yield('title')"  data-sbg-source="Aakil Fernandes"  data-sbg-summary=""  data-sbg-width="585"  data-sbg-height="471" >
        <i class="sbg-button-icon fa fa-linkedin"></i>
      </a>
            
      <a class="sbg-button sbg-button-google-plus"  data-sbg-network="google-plus"  data-sbg-url="{{Request::url()}}"  data-sbg-width="500"  data-sbg-height="505" >
        <i class="sbg-button-icon fa fa-google-plus"></i>
      </a>
            
      <a class="sbg-button sbg-button-email"  data-sbg-network="email"  data-sbg-subject="@yield('title')"  data-sbg-body="{{Request::url()}}" >
        <i class="sbg-button-icon fa fa-envelope"></i>
      </a>
    </div>
    <div class="container">
      <div id="disqus_thread"></div>
      <script type="text/javascript">
          /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
          var disqus_shortname = 'aakilfernandes'; // required: replace example with your forum shortname

          /* * * DON'T EDIT BELOW THIS LINE * * */
          (function() {
              var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
              dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
              (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
          })();
      </script>
      <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
      <a href="http://disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>
    </div>
    {{HTML::script('https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js')}}
    {{HTML::script('//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.2.0/js/bootstrap.min.js')}}
    {{HTML::script('/js/sbg.js')}}
    @yield('footer')
  </body>
</html>