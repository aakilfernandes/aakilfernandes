@extends('layout')
	

@section('content')
<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-2"><h3>About Me</h3></div>
		<div class="col-xs-12 col-sm-10 section-content">
			I'm a full stack web developer based in New York City. I'm not currently looking for work.
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-2"><h3>Skills</h3></div>
		<div class="col-xs-12 col-sm-10 section-content">
			HTML(5), CSS(3), Bootstrap, Javascript, jQuery, jQuery UI, jQuery Mobile, Underscore.js, JSON, AJAX, Backbone.js, PHP, mySQL, Laravel, Cordova/PhoneGap, Git, Photoshop
		</div>
	</div>
	<!--
	<hr>
	<div class="row">
		<div class="col-xs-12 col-sm-2">
			<h3>Projects</h3>
			<div class="filters">
				<button class="btn btn-default btn-primary" data-filter="all">All</button>
				<button class="btn btn-default" data-filter="backbone">Backbone.js</button>
				<button class="btn btn-default" data-filter="bootstrap">Bootstrap</button>
				<button class="btn btn-default" data-filter="foundation">Foundation</button>
				<button class="btn btn-default" data-filter="php">PHP/mySql</button>
				<button class="btn btn-default" data-filter="laravel">Laravel</button>
				<button class="btn btn-default" data-filter="phonegap">Phonegap</button>
				<button class="btn btn-default" data-filter="jquerymobile">jQuery Mobile</button>
				<button class="btn btn-default" data-filter="f7">Framework7</button>
			</div>
		</div>
		<div class="col-xs-12 col-sm-10 section-content">
			<div class="projects row">
				<div class="project col-xs-12 col-sm-6" data-filters="php laravel backbone bootstrap"><div class="project-wrapper">
				  	<a href="http://karmalytics.co"><h6>Karmalytics</h6></a>
				  	<p>A brand monitoring tool for Reddit and Hacker News.</p>
				  	<div class="project-screens">
					  	<img class="project-screen" src="/img/screens/karmalytics/landing.png">
					  	<img class="project-screen" src="/img/screens/karmalytics/dashboard.png">
					</div>
			  	</div></div>
				<div class="project col-xs-12 col-sm-6" data-filters="php laravel backbone bootstrap"><div class="project-wrapper">
				  	<a href="http://nichekick.com"><h6>NicheKick</h6></a>
				  	<p>Cool stuff voted on by Redditors.</p>
				  	<img class="project-screen" src="/img/screens/nichekick/landing.png">
			  	</div></div>
			  	<div class="project col-xs-12 col-sm-6" data-filters="f7"><div class="project-wrapper">
				  	<h6>Construction App</h6>
				  	<p>A hybrid mobile app for a construction company (available upon request)</p>
				  	<div class="project-screens">
					  	<img class="project-screen" src="/img/screens/gate/landing.png">
					  	<img class="project-screen" src="/img/screens/gate/album.png">
					  </div>
			  	</div></div>
			  	<div class="project col-xs-12 col-sm-6" data-filters="foundation"><div class="project-wrapper">
				  	<a href="http://therunthrough.com"><h6>The Runthrough</h6></a>
				  	<p>A platform for fashion professionals to connect and share inspiration.</p>
				  	<img class="project-screen" src="/img/screens/therunthrough/landing.png">
			  	</div></div>
				<div class="project col-xs-12 col-sm-6" data-filters="backbone php bootstrap"><div class="project-wrapper">
				  	<a href="http://unclebobscoupons.com"><h6>Uncle Bob's Coupons</h6></a>	
				  	<img class="project-screen" src="/img/screens/ubc/landing.png">
			  	</div></div>
				<div class="project col-xs-12 col-sm-6" data-filters="backbone php phonegap jquerymobile"><div class="project-wrapper">
				  	<a href="http://gemsinthejungle.com"><h6>Gems In The Jungle</h6></a>
				  	<p>GITJ is a product curation site. I built the site from the ground up, including amazon integrations. The iOS app is built on jQuery Mobile, Backbone, and Phonegap/Cordova</p>
				  	<div class="project-screens">
					  	<img class="project-screen" src="/img/screens/gitj/landing.png">
					  	<img class="project-screen" src="/img/screens/gitj-app/landing.jpeg">
					  	<img class="project-screen" src="/img/screens/gitj-app/product.jpeg">
					  	<img class="project-screen" src="/img/screens/gitj-app/browser.jpeg">
					</div>
			  	</div></div>
			  	<div class="project col-xs-12 col-sm-6" data-filters="d3 php"><div class="project-wrapper">
				  	<a href="http://techmap.codeharmony.net"><h6>Tech Map</h6></a>
				  	<p>Users can play around with data, and compare tech usage in different hubs around the world. The project uses SVGs and D3.js to make tech trends easy to understand.</p>
				  	<img class="project-screen" src="/img/screens/codeharmony/techmap.png">
			  	</div></div>
			  	<div class="project col-xs-12 col-sm-6" data-filters="backbone bootstrap"><div class="project-wrapper">
				  	<a href="http://sharebuttongenerator.aakilfernandes.com"><h6>Share Button Generator</h6></a>
				  	<p>If you want to add share buttons to your site you have two options: Use a 3rd party plugins or spend a half hour looking up and testing social network APIs. Both of those are annoying, so I built a generator which takes all the grunt work out of making share buttons.</p>
				  	<img class="project-screen" src="/img/screens/sbg/landing.png">
			  	</div></div>
			  	<div class="project col-xs-12 col-sm-6" data-filters="php elasticsearch"><div class="project-wrapper">
				  	<a href="http://codeharmony.net"><h6>Code Harmony</h6></a>
				  	<p>CodeHarmony is a search engine for coders. Users input a location and skills, and CodeHarmony searches for relevant coders. I built the entire site: included an API based data scraper and the PHP back end. I also migrated a mySQL database to a hosted ElasticSearch cluster.</p>
					<div class="project-screens">
					  	<img class="project-screen" src="/img/screens/codeharmony/landing.png">
					  	<img class="project-screen" src="/img/screens/codeharmony/search.png">
					</div>
			  	</div></div>
			</div>
		</div>
	</div>
	-->
</div>
@stop

@section('footer')
	{{HTML::script('//cdnjs.cloudflare.com/ajax/libs/jquery.isotope/2.0.0/isotope.pkgd.min.js')}}
	{{HTML::script('/js/page/index.js')}}
@stop
