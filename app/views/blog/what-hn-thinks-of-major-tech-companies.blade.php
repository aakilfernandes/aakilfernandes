@extends('blog')

@section('title','What HN Thinks of Major Tech Companies')

@section('content')
	<div class="container">
		<h2>What HN Thinks of Major Tech Companies</h2>
		<p>I run a useful little keyword tracker at <a href="http://karmalytics.co">Karmalytics.co</a> that helps businesses and startups see who's talking about them on Reddit and Hacker News. If you like this post, you might want to check it out.</p>
		<p>For this post, I looked at comments from Hacker News containing any of the following keywords:</p>
		<ul>
			<li>Amazon</li>
			<li>Apple</li>
			<li>Facebook</li>
			<li>Google</li>
			<li>Microsoft</li>
		</ul>
		<p>And ran them through the sentiment analysis tool provided by <a href="http://alchemyapi.com">AlchemyAPI</a>. Here's what I found:</p>
		<iframe width="640" height="480" frameborder="0" seamless="seamless" scrolling="no" src="https://plot.ly/~aakilfernandes/0/640/480"></iframe>
		<p>The results aren't too surprising to me in terms of direction, but the magnitude is pretty interesting. Hatred for Apple seems to run even deper than I imagined.</p>
		<h3>Data set + sample sizes</h3>
		<p>You can grab the full dataset here from my <a href="https://bitbucket.org/aakilfernandes/aakilfernandes/src/d3d1c60b6360/datasets/?at=master">bitbucket</a>. The sample sizes are included below.</p>
		<table class="table">
			<tr>
				<td>amazon</td><td>404</td>
			</tr><tr>
				<td>apple</td><td>536</td>
			</tr><tr>
				<td>facebook</td><td>882</td>
			</tr><tr>
				<td>google</td><td>1009</td>
			</tr><tr>
				<td>microsoft</td><td>843</td>
			</tr>
		</table>
	</div>
@stop