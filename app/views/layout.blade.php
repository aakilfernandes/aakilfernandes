<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aakil Fernandes</title>
    {{HTML::style('http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600')}}
    {{HTML::style('//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.2.0/css/bootstrap.min.css')}}
    {{HTML::style('/css/style.css')}}
    {{HTML::style('/css/page/index.css')}}
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    {{HTML::script('/js/lib/ga.js')}}
    <header>
        <div class="container">
            <h1>Aakil Fernandes<span class="hidden-xs"> - NYC</span></h1>
            <div class="header-tagline">Full stack web developer</div>
            <div class="header-links">
                <a href="mailto:aakilfernandes@gmail.com">aakilfernandes@gmail.com</a>,
                <a href="https://www.linkedin.com/in/aakilfernandes">linkedin</a>,
                <a href="/resume">resume</a>,
                 <a href="https://bitbucket.org/aakilfernandes/">bitbucket</a>
            </div>
        </div>
    </header>
    @yield('content')
    {{HTML::script('https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js')}}
    {{HTML::script('//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.2.0/js/bootstrap.min.js')}}
    @yield('footer')
  </body>
</html>